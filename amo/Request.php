<?php


namespace amo;


abstract class Request
{
    protected $config = [];
    protected $url = '';

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->config = require __DIR__ . '/../config/conf.php';
        $this->url = 'https://' . $this->config['subdomain'] . '.amocrm.ru/api/v2/';
        if (!$this->auth()) {
            header('HTTP/1.0 403 Forbidden');
            die('Нет авторизации на Амо');
        }
    }


    /**
     * @param string $link Url
     * @param array $data массив данных запроса
     * @param array $headers массив заголовков
     * @param string $method метод
     * @return array Ответ сервера
     */
    public function curl(string $link, array $data, array $headers = [], string $method = 'POST')
    {
        //Обработки ошибок Amo нет. Тестовое задание все же.
        //Тут полагаю все ясно
        array_push($headers, "Content-Type: application/json");
        $curl = curl_init();
        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'GET':
                $link = $link . '/?' . http_build_query($data);
                break;
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return json_decode($out, true);
    }

    /**
     * Авторизация на сервере Amo
     * @return boolean Успешно или нет
     */
    public function auth()
    {
        $user = [
            'USER_LOGIN' => $this->config['login'],
            'USER_HASH' => $this->config['hash'],
        ];
        $link = 'https://' . $this->config['subdomain'] . '.amocrm.ru/private/api/auth.php?type=json';
        $response = $this->curl($link, $user);
        return isset($response['response']['auth']) ? $response['response']['auth'] : false;
    }
}
