<?php
/**
 * Боже, как это будет работать при паре миллионов записей, понятия не имею.
 * На больших массивах не проверял. Видимо придется делить по IF-MODIFIED-SINCE например.
 */

namespace amo;


abstract class AbstractAmo extends Request
{
    /**
     * AbstractAmo constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $items массив записей для создания по мануалу для сущности
     * @return array Ответ сервера
     */
    protected function add(array $items): array
    {
        $list = [];
        if (count($items) > 500) { //Ограничение Amo на создание сущностей не более 500
            $chunks = array_chunk($items, 500);
            $count = 0;
            foreach ($chunks as $chunk) {
                $range = $this->curl($this->url, ['add' => $chunk]);
                $list = array_merge($list, !empty($range['_embedded']['items']) ? $range['_embedded']['items'] : []);
                $count++;
                if ($count % 7 == 0) sleep(1); //Ограничение 7 запросов в секунду, забанят
            }
        } else {
            $one = $this->curl($this->url, ['add' => $items]);
            $list = !empty($one['_embedded']['items']) ? $one['_embedded']['items'] : [];
        }
        return $list;
    }


    /**
     * Получение всех записей без учета страниц при паре миллионов сожрет память и умрет
     * @param string|null $modifyData Дата изменения
     * @param array $items
     * @param int $limit_rows
     * @param int $limit_offset
     * @param int $counter
     * @return array Ответ сервера
     */
    protected function all(string $modifyData = null, $items = [], $limit_rows = 500, $limit_offset = 0, $counter = 0): array
    {
        $modifyData = $this->modifiedHeader($modifyData);
        $headers = $modifyData ? [$modifyData] : [];
        $list = $this->curl($this->url, ['limit_rows' => $limit_rows, 'limit_offset' => $limit_offset], $headers, 'GET');
        $count = isset($list['_embedded']['items']) ? count($list['_embedded']['items']) : 0;
        if ($count) {
            $limit_offset = $limit_offset + $count;
            $items = array_merge($items, !empty($list['_embedded']['items']) ? $list['_embedded']['items'] : []);
            $counter++;
            if ($counter % 7 == 0) sleep(1);
            return $this->all($modifyData, $items, $limit_rows, $limit_offset, $counter);
        } else {
            return $items;
        }
    }

    /**
     * Общее количество записей
     * @param int $totalCount
     * @param int $limit_rows
     * @param int $limit_offset
     * @return int
     */
    protected function countItems($totalCount = 0, $limit_rows = 500, $limit_offset = 0, $counter = 0): int
    {
        $list = $this->curl($this->url, ['limit_rows' => $limit_rows, 'limit_offset' => $limit_offset], [], 'GET');
        $count = isset($list['_embedded']['items']) ? count($list['_embedded']['items']) : 0;
        if ($count) {
            $totalCount = $totalCount + $count;
            $counter++;
            if ($counter % 7 == 0) sleep(1);
            return $this->countItems($totalCount, $limit_rows, $totalCount, $counter);
        } else {
            return $totalCount;
        }
    }


    /**
     * Количество страниц, почему то нормальной пагинации нет в API
     * @param int $limit Количество записей на странице для текущего функционала на нужно,
     * но пусть будет, вдруг потом понадобится.
     * @return int
     */
    protected function pages(int $limit = 500): int
    {
        return ceil($this->countItems() / $limit);
    }


    /**
     * Аналогично методу all() только с фильтрацией
     * @param array $filter Массив условий фильтрации
     * @param string|null $modify Дата изменения
     * @return array Ответ сервера
     */
    protected function filter(array $filter, string $modify = null): array
    {
        $modify = $this->modifiedHeader($modify);
        $headers = $modify ? [$modify] : [];
        $pages = $this->pages();
        $list = [];
        $counter = 0;
        for ($i = 0; $i < $pages; $i++) {
            $pageFilter = $this->curl($this->url, ['filter' => $filter, 'limit_rows' => 500, 'limit_offset' => 500 * $i], $headers, 'GET');
            $list = array_merge($list, !empty($pageFilter['_embedded']['items']) ? $pageFilter['_embedded']['items'] : []);
            $counter++;
            if ($counter % 7 == 0) sleep(1);
        }
        return $list;
    }

    private function modifiedHeader($date): string
    {
        $modify = (bool)strtotime($date) ? (new \DateTime($date))->format(\DateTime::RFC2822) : null;
        return $modify ? 'IF-MODIFIED-SINCE: ' . $modify : '';
    }


}