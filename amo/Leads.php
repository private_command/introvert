<?php

namespace amo;


class Leads extends AbstractAmo
{
    /**
     * Leads constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->url = $this->url . 'leads';
    }

    public function all($modifyData = null, $items = [], $limit_rows = 500, $limit_offset = 0, $counter = 0): array
    {
        return parent::all($modifyData, $items, $limit_rows, $limit_offset, $counter);
    }

    /**
     * @param array $filter Массив условий фильтрации
     * @return array Массив найденных сделок
     */
    public function filter(array $filter, string $modify = null): array
    {
        return parent::filter($filter, $modify);
    }

}
