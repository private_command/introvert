<?php


namespace amo;


class Tasks extends AbstractAmo
{
    /**
     * Tasks constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->url = $this->url . 'tasks';
    }

    /**
     * @param array $tasks массив задач
     * @return array массив ответа сервера
     */
    public function add(array $tasks): array
    {
        return parent::add($tasks);
    }
}