<?php


namespace amo;


class Log
{
    /**
     * @param array $data Массив для записи
     * @param string $title Заголовок записи
     * @param string $path Путь к файлу лога
     * @return bool
     * @throws \Exception
     */
    public function writeArray(array $data, string $title = '', string $path = ''): bool
    {
        $log = "\n------------------------\n";
        $log .= (new \DateTime())->format('d-m-Y H:i:s') . "\n";
        $log .= (strlen($title) > 0 ? $title : 'DEBUG') . "\n";
        $log .= print_r($data, 1);
        $log .= "\n------------------------\n";
        return file_put_contents($path, $log, FILE_APPEND);
    }
}